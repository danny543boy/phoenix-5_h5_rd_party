# 遊戲前端-保龍一族聚會
![餐點圖片](./image/Screenshot_20211024-112756_Skype.jpeg)

## 聚會資訊

- 地點 : KOI PLUS (七期店)
- 地址 : 臺中市西屯區市政北二路50號2樓
- 時間 : 2021年11月4日 晚上18:15
- Google地圖連結 : [連結](https://g.page/KOI-PLUS?share)
- Facebook粉絲專頁連結 : [連結](https://www.facebook.com/KOI-PLUS-105643488484199)
- 菜單連結 : [連結](https://www.facebook.com/media/set/?vanity=koiplustw&set=a.2419735984825101)

![餐點圖片](./image/SmartSelect_20211024-113531_Maps.jpeg)

餐廳外有杯大珍奶很好認 ( 照片來源:Goolge地圖 )

![餐點圖片](./image/244942293_620465902452574_602858783310468531_n.jpeg)



餐點示意圖 [(IG連結)](https://www.instagram.com/p/CU4Y2VUlWdZ/?utm_source=ig_web_copy_link)